﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Messages;

namespace SendMessage
{
  internal static class Program
  {
    private const string RabbitMqBase = "rabbitmq://localhost";

    private static async Task Main(string[] args)
    {
      var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
      {
        sbc.Host(new Uri(RabbitMqBase), configurator =>
        {
          configurator.Username("guest");
          configurator.Password("guest");
        });
      });

      await bus.StartAsync();

      var sendEndpoint = await bus.GetSendEndpoint(new Uri(RabbitMqBase + "/new_message_queue"));
      var messageId = 1;
      while (true)
      {
        Console.WriteLine("Enter your message: ");
        var textMessage = Console.ReadLine();

        if (string.IsNullOrWhiteSpace(textMessage)) continue;
        if (textMessage == "exit") break;

        var message = new Message(++messageId, DateTime.Now, textMessage);
        // await bus.Publish(message);

        await sendEndpoint.Send(message);
      }

      await bus.StopAsync();
    }
  }
}