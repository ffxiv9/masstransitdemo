﻿using System;
using System.Threading.Tasks;
using MassTransit;

namespace ReceiveMessage2
{
  internal static class Program
  {
    private static async Task Main(string[] args)
    {
      var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
      {
        sbc.Host(new Uri("rabbitmq://localhost"), configurator =>
        {
          configurator.Username("guest");
          configurator.Password("guest");
        });
        sbc.ReceiveEndpoint("new_message_queue2", configurator => { configurator.Consumer<MessageConsumer2>(); });
      });

      await bus.StartAsync();

      Console.WriteLine("Press any key to exit");
      Console.ReadKey();
      await bus.StopAsync();
    }
  }
}