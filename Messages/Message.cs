﻿using System;

namespace Messages
{
  public class Message
  {
    public Message(int id, DateTime time, string content)
    {
      Id = id;
      Time = time;
      Content = content;
    }

    public int Id { get; set; }
    public DateTime Time { get; set; }
    public string Content { get; set; }
  }
}