﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Messages;

namespace ReceiveMessage2
{
  public class MessageConsumer2 : IConsumer<Message>
  {
    public async Task Consume(ConsumeContext<Message> context)
    {
      await Console.Out.WriteLineAsync(
        $"Received in second consumer: {context.Message.Content} {context.Message.Time}");
    }
  }
}