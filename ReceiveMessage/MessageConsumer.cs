﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Messages;

namespace ReceiveMessage
{
  public class MessageConsumer : IConsumer<Message>
  {
    public async Task Consume(ConsumeContext<Message> context)
    {
      await Console.Out.WriteLineAsync($"Received in first consumer: {context.Message.Content} {context.Message.Time}");
    }
  }
}